from DataSaver import DataSaver

import tensorflow as tf
import numpy as np
import logging


FRAME_CROP_PARAMS = [[32, 181], [28, 132], [0, 1]]
# https://towardsdatascience.com/double-deep-q-networks-905dd8325412


class Brain:
    def __init__(self, learning_rate, gamma, n_actions, number_of_frames=4, epsilon_step=2e-6, epsilon=0.1,
                 batch_size=64, memory_size=4096, parameters_exchange_time=1, parameters_exchange_smoothing=0.01,
                 load_brain_path=None, clipnorm=1e-1, number_of_batches=8):
        assert batch_size <= memory_size
        self.data_saver = DataSaver()
        self.learning_rate = learning_rate
        self.clipnorm = clipnorm
        self.gamma = gamma
        self.frame_size = [FRAME_CROP_PARAMS[0][1] - FRAME_CROP_PARAMS[0][0],
                           FRAME_CROP_PARAMS[1][1] - FRAME_CROP_PARAMS[1][0],
                           number_of_frames]
        self.n_actions = n_actions
        self.epsilon = epsilon
        self.epsilon_step = epsilon_step
        self.batch_size = batch_size
        self.number_of_batches = number_of_batches
        self.memory_size = memory_size
        self.parameters_exchange_time = parameters_exchange_time
        self.parameters_exchange_smoothing = parameters_exchange_smoothing
        self.last_reward = 0

        self.score = dict(opponent_score=0, player_score=0)
        self.experience_counter = 0
        self.experience_counter_max = 0
        self.learning_counter = 0
        self.frames_memory_old = np.zeros((self.memory_size, *self.frame_size))
        self.frames_memory_current = np.zeros((self.memory_size, *self.frame_size))
        self.actions_rewards_memory = np.zeros((self.memory_size, 2))

        if load_brain_path is None:
            self.action_value_function, self.target_action_value_function = self._build_networks()
        else:
            self.action_value_function = tf.keras.models.load_model(load_brain_path, compile=False)
            self.target_action_value_function = tf.keras.models.load_model(load_brain_path, compile=False)
            self.action_value_function.compile(
                loss=self._custom_loss,
                optimizer=tf.keras.optimizers.SGD(lr=self.learning_rate, clipnorm=self.clipnorm))
            self.target_action_value_function.compile(
                loss=self._custom_loss,
                optimizer=tf.keras.optimizers.SGD(lr=self.learning_rate, clipnorm=self.clipnorm))

    def save_brain(self, save_brain_path):
        tf.keras.models.save_model(self.target_action_value_function, save_brain_path)

    def reset_score(self):
        self.score = dict(opponent_score=0, player_score=0)

    def _build_networks(self):

        input_layer = tf.keras.layers.Input(shape=self.frame_size)
        x = tf.keras.layers.Conv2D(filters=64, kernel_size=(3, 3), activation=tf.nn.swish,
                                   kernel_initializer='glorot_normal', bias_initializer='glorot_normal')(input_layer)
        x = tf.keras.layers.Conv2D(filters=64, kernel_size=(3, 3), activation=tf.nn.swish,
                                   kernel_initializer='glorot_normal', bias_initializer='glorot_normal')(x)
        x = tf.keras.layers.MaxPool2D(pool_size=(3, 2))(x)
        x = tf.keras.layers.Conv2D(filters=64, kernel_size=(3, 3), activation=tf.nn.swish,
                                   kernel_initializer='glorot_normal', bias_initializer='glorot_normal')(x)
        x = tf.keras.layers.Conv2D(filters=128, kernel_size=(3, 3), activation=tf.nn.swish,
                                   kernel_initializer='glorot_normal', bias_initializer='glorot_normal')(x)
        x = tf.keras.layers.MaxPool2D(pool_size=(2, 2))(x)
        x = tf.keras.layers.Conv2D(filters=128, kernel_size=(3, 3), activation=tf.nn.swish,
                                   kernel_initializer='glorot_normal', bias_initializer='glorot_normal')(x)
        x = tf.keras.layers.Conv2D(filters=256, kernel_size=(3, 3), activation=tf.nn.swish,
                                   kernel_initializer='glorot_normal', bias_initializer='glorot_normal')(x)
        x = tf.keras.layers.MaxPooling2D(pool_size=(2, 2))(x)
        x = tf.keras.layers.Conv2D(filters=256, kernel_size=(3, 3), activation=tf.nn.swish,
                                   kernel_initializer='glorot_normal', bias_initializer='glorot_normal')(x)
        x = tf.keras.layers.Conv2D(filters=256, kernel_size=(3, 3), activation=tf.nn.swish,
                                   kernel_initializer='glorot_normal', bias_initializer='glorot_normal')(x)

        x = tf.keras.layers.Flatten()(x)

        x = tf.keras.layers.Dense(256, activation=tf.nn.swish,
                                  kernel_initializer='glorot_normal', bias_initializer='glorot_normal')(x)
        output_layer = tf.keras.layers.Dense(
            self.n_actions, kernel_initializer='glorot_normal', bias_initializer='glorot_normal')(x)

        action_value_function = tf.keras.Model(inputs=input_layer, outputs=output_layer)
        action_value_function.compile(loss=self._custom_loss,
                                      optimizer=tf.keras.optimizers.SGD(lr=self.learning_rate, clipnorm=self.clipnorm))

        logging.info('Model was built:')
        action_value_function.summary(print_fn=logging.info)

        target_action_value_function = tf.keras.models.clone_model(action_value_function)

        return action_value_function, target_action_value_function

    @staticmethod
    def _custom_loss(y_true, q_pred):
        q_true, r = tf.split(y_true, 2, axis=1)
        return (1. + 9. * tf.abs(r)) * tf.keras.losses.Huber()(q_true, q_pred)

    def store_experience(self, old_state, a, r, current_state):
        self.frames_memory_old[self.experience_counter] = old_state
        self.actions_rewards_memory[self.experience_counter] = [a, r]
        self.frames_memory_current[self.experience_counter] = current_state
        self.experience_counter = (self.experience_counter + 1) % self.memory_size
        self.experience_counter_max = min(self.experience_counter_max + 1, self.memory_size)

    def fit(self):
        indices = np.random.choice(self.experience_counter_max, self.number_of_batches * self.batch_size)
        old_states = self.frames_memory_old[indices]
        current_states = self.frames_memory_current[indices]
        current_actions, current_rewards = self.actions_rewards_memory[indices].T

        action_max = self.action_value_function.predict(x=old_states).argmax(axis=1)
        q_target = current_rewards + self.gamma * np.min([
            self.action_value_function.predict(x=current_states)[np.arange(len(indices)), action_max],
            self.target_action_value_function.predict(x=current_states)[np.arange(len(indices)), action_max]
        ], axis=0)

        loss = self.action_value_function.fit(
            x=old_states,
            y=np.concatenate((q_target[:, np.newaxis], current_rewards[:, np.newaxis]), axis=1),
            batch_size=self.batch_size,
            verbose=0
        ).history['loss'][0]

        print(f'Reward {self.last_reward:.4f}\t'
              f'Score {self.score}\t'
              f'Epsilon {self.epsilon:.4f}\t'
              f'Loss {loss:20.4f}\t', end='\r')

        # increasing epsilon  - epsilon greedy exploration is decreasing in favor of network's decisions
        if self.epsilon < 1:
            self.epsilon += self.epsilon_step

        if self.learning_counter > self.parameters_exchange_time:
            self.target_action_value_function.set_weights(
                [self.parameters_exchange_smoothing * i + (1 - self.parameters_exchange_smoothing) * j for i, j in
                 zip(self.action_value_function.get_weights(), self.target_action_value_function.get_weights())])
            # logging.info('Target network weights have been changed')
            self.learning_counter = 0
        else:
            self.learning_counter += 1
        self.data_saver.insert_data_with_scores(loss, self.last_reward, **self.score)

    def epsilon_greedy_prediction(self, obs):
        if np.random.uniform(low=0, high=1) < self.epsilon:
            predictions = self.target_action_value_function.predict(obs[np.newaxis])
            return np.argmax(predictions)
        else:
            return np.random.choice(self.n_actions)
