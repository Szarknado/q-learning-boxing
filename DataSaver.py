import pandas as pd


class DataSaver:
    def __init__(self, save_freq: int = 1000):
        self._save_freq = save_freq
        self._saved_n_data = self._save_freq
        self._n_partition_data = 0
        self._data = None

    def __exit__(self, exc_type, exc_val, exc_tb):
        if self._data is not None:
            self._save_data(force_save=True)

    @property
    def data(self):
        if self._data is None:
            self._data = pd.DataFrame(columns='loss reward opponent_score player_score'.split())
        return self._data

    def _save_data(self, force_save: bool = False):
        self._saved_n_data = (self._saved_n_data + 1) % self._save_freq
        if self._saved_n_data == 0 or force_save:
            self.data.to_csv(f'data/data_{self._n_partition_data}.csv', index=False)
            self._n_partition_data += 1
            self._data = None

    def insert_data_with_scores(self, loss=None, reward=None, opponent_score=None, player_score=None):
        self._data = self.data.append(
            {'loss': loss, 'reward': reward, 'opponent_score': opponent_score, 'player_score': player_score},
            ignore_index=True)
        self._save_data()
