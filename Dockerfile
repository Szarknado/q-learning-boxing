FROM tensorflow/tensorflow:2.3.1-gpu-jupyter
COPY requirements.txt /tf
RUN apt update && apt install -y libglu1-mesa-dev libgl1-mesa-dev libosmesa6-dev xvfb ffmpeg curl patchelf libglfw3 libglfw3-dev cmake zlib1g zlib1g-dev swig && pip install --upgrade pip && pip install -r /tf/requirements.txt