import cv2
import gym
import logging
import numpy as np

from Brain import Brain, FRAME_CROP_PARAMS


logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO)


def render_observations(observations, number_of_frames):
    try:
        frame = cv2.cvtColor(
            (1. + np.concatenate([j for i, j in enumerate(observations) if i in number_of_frames], axis=1).astype('float32')) / 2.,
            cv2.COLOR_BGR2RGB)

        cv2.imshow('Location channel | Movement channel', frame)
        cv2.waitKey(1)
    except KeyboardInterrupt:
        cv2.destroyAllWindows()


def render_raw_observation(obs_raw):
    try:
        cv2.imshow('Raw', obs_raw)
        cv2.waitKey(1)
    except KeyboardInterrupt:
        cv2.destroyAllWindows()


def render_q_values(q_values):
    try:
        q_values = 255 * (q_values - q_values.min()) / (q_values.max() - q_values.min())
        cv2.imshow('Q-Values', q_values)
        cv2.waitKey(1)
    except KeyboardInterrupt:
        cv2.destroyAllWindows()


def run_single_episode(env, brain, episode, number_of_frames,
                       force_fit=False, render=False, raw_render=False, q_values_render=False,
                       gamma_exp_smooth=0.15, learn_every_n_steps=64, fit_model=True):
    dead = episode_reward = 0
    steps = 1
    observations = []
    observations_smoothed = []
    number_of_frames = np.array(number_of_frames)
    number_of_frames = max(number_of_frames) - number_of_frames
    number_of_frames_index = np.concatenate((number_of_frames, max(number_of_frames) + 1 + number_of_frames))

    obs_new_raw_tmp = _norm_frame(_crop_frame(env.reset()))
    observations.append(obs_new_raw_tmp.copy())
    observations_smoothed.append(obs_new_raw_tmp.copy())
    for _ in range(max(number_of_frames)):
        obs_new_raw_tmp = _norm_frame(_crop_frame(env.step(0)[0]))
        observations.append(obs_new_raw_tmp.copy())
        observations_smoothed.append(_frame_exp_smoothing(observations_smoothed[-1], obs_new_raw_tmp.copy(), gamma_exp_smooth))
    obs_new = np.concatenate(observations + observations_smoothed, axis=-1)[..., number_of_frames_index]

    while not dead:
        obs_old = obs_new.copy()
        action = brain.epsilon_greedy_prediction(obs_old)
        obs_new_raw, reward, dead, _ = env.step(action)
        obs_new_raw_tmp = _norm_frame(_crop_frame(obs_new_raw))
        observations.append(obs_new_raw_tmp.copy())
        observations_smoothed.append(_frame_exp_smoothing(observations_smoothed[-1], obs_new_raw_tmp.copy(), gamma_exp_smooth))
        observations.pop(0)
        observations_smoothed.pop(0)
        obs_new = np.concatenate(observations + observations_smoothed, axis=-1)[..., number_of_frames_index]
        _update_reward_list(brain, reward)
        brain.last_reward = reward

        if raw_render:
            render_raw_observation(obs_new_raw)
        if render:
            render_observations(observations + observations_smoothed, number_of_frames_index)
        if q_values_render:
            render_q_values(brain.target_action_value_function.predict(obs_new[np.newaxis]))

        brain.store_experience(obs_old, action, reward, obs_new)
        if reward in [-1, 1]:
            _update_reward_memory(brain, reward, gamma_exp_smooth)

        if (force_fit or steps % learn_every_n_steps == 0 or dead) and fit_model:
            brain.fit()

        episode_reward += reward
        steps += 1
    logging.info(f"Episode {episode + 1} ended with reward {episode_reward} at epsilon {brain.epsilon}")
    brain.reset_score()
    return env, brain


def _norm_frame(frame):
    if np.isclose(frame.max(), frame.min()):
        return np.zeros_like(frame)
    return 2 * (frame - frame.min()) / (frame.max() - frame.min()) - 1


def _crop_frame(frame):
    # return frame[:, np.newaxis]
    return frame[FRAME_CROP_PARAMS[0][0]:FRAME_CROP_PARAMS[0][1],
           FRAME_CROP_PARAMS[1][0]:FRAME_CROP_PARAMS[1][1],
           FRAME_CROP_PARAMS[2][0]:FRAME_CROP_PARAMS[2][1]]


def _frame_exp_smoothing(smoothed_frame, new_frame, gamma_exp_smooth):
    return _norm_frame(smoothed_frame * (1 - gamma_exp_smooth) + new_frame * gamma_exp_smooth)


def _update_reward_memory(brain, reward, gamma_exp_smooth):
    memory_id = (brain.experience_counter - 2) % brain.memory_size
    while brain.actions_rewards_memory[memory_id, 1] == 0:
        reward *= gamma_exp_smooth
        brain.actions_rewards_memory[memory_id, 1] = reward
        if np.isclose(reward, 0.):
            break
        memory_id = (memory_id - 1) % brain.memory_size


def _update_reward_list(brain, reward):
    if reward == -1:
        brain.score['opponent_score'] += 1
    elif reward == 1:
        brain.score['player_score'] += 1


if __name__ == "__main__":
    number_of_frames = [0, 1, 2, 3]

    env = gym.make('Boxing-v0', frameskip=1)
    # env = gym.make('CartPole-v1')
    observation = env.reset()

    print('Frames', env.observation_space.shape, "cropped to", FRAME_CROP_PARAMS)

    brain = Brain(learning_rate=0.1, gamma=0.9, n_actions=env.action_space.n, epsilon=1.3, epsilon_step=1e-3,
                  number_of_frames=2 * len(number_of_frames), load_brain_path='brains/brain_400.h5')
    episodes = 1

    for episode in range(episodes):
        env, brain = run_single_episode(env, brain, episode, number_of_frames,
                                        render=True, raw_render=True, q_values_render=True, fit_model=False)
        # brain.save_brain(f"brains/brain_{episode}")
    cv2.destroyAllWindows()
